﻿using System;

namespace SimpleClassConlsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.Unicode;
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");

            Console.Title = "Лабораторна робота №7";
            Console.SetWindowSize(100, 25);

            Console.BackgroundColor = ConsoleColor.White;
            Console.ForegroundColor = ConsoleColor.DarkBlue;
            Console.Clear();

            Console.WriteLine("Лабораторна робота №7");
            Console.WriteLine("Виконала Попеску В.С СН-21");
            Console.WriteLine("Варіант №18");

            Console.ReadKey();

            Console.ResetColor();

            Console.Clear();

            Worker[] person;
            Console.WriteLine("Введіть кількість елементів: ");
            int n = Convert.ToInt32(Console.ReadLine());
            person = new Worker[n];

            int menu = 0;

            Console.ForegroundColor = ConsoleColor.Cyan;

            while (true)
            {
                if (menu != 0) Console.ReadKey();
                Console.Clear();


                Console.WriteLine("\tМеню\n");
                Console.WriteLine("1) Записати дані в масив;");
                Console.WriteLine("2) Знайти масив за номером;");
                Console.WriteLine("3) Вивести всі елементи масиву;");
                Console.WriteLine("4) Вивести максимальну та мінімальну зарплату працівників;");
                Console.WriteLine("5) Сортування масиву структур за спаданням зарплати; ");
                Console.WriteLine("6) Сортування масиву структур за зростанням стажу роботи; ");
                Console.WriteLine("7) Вихід з програми");
                Console.WriteLine();
                Console.Write("Номер > ");
                menu = Convert.ToInt32(Console.ReadLine());

                Console.Clear();

                if (menu == 1) ReadWorkersArray(person, n);
                if (menu == 2) PrintWorker(person, 3);
                if (menu == 3) PrintWorkers(person, n);

                if (menu == 4)
                {
                    int max_salary = 0;
                    int min_salary = 0;
                    Console.ForegroundColor = ConsoleColor.Green;
                    GetWorkersInfo(person, n, out max_salary, out min_salary);
                    Console.WriteLine("Максимальна зарплата: " + max_salary);
                    Console.WriteLine("Мінімальна зарплата: " + min_salary);
                    Console.ForegroundColor = ConsoleColor.Cyan;
                }

                if (menu == 5)
                {
                    SortWorkerBySalary(person);
                    Console.WriteLine("\n\nМасив відсортовано.");
                }
                if (menu == 6)
                {
                    SortWorkerByWorkExperience(person);
                    Console.WriteLine("\n\nМасив відсортовано.");
                }
                if (menu == 7) break;

                if (menu < 1 || menu > 7)
                {
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine("ERROR: Введіть значення від 1 до 7");
                    menu = 0;
                    Console.ReadKey();
                    Console.ForegroundColor = ConsoleColor.Cyan;
                }
            }

            Worker[] manager = person; // Конструктор копіювання
        }

        static void ReadWorkersArray(Worker[] Array, int number)
        {
            for(int i = 0; i<number; i++)
            {
                Console.WriteLine("Введіть прізвище та ініціали працівника: ");
                Array[i].name = Console.ReadLine();
                Console.WriteLine("Введіть рік початку роботи: ");
                Array[i].year = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Введіть місяць початку роботи: ");
                Array[i].month = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Введіть назву компанії: ");
                Array[i].workPlace.name = Console.ReadLine();
                Console.WriteLine("Введіть посаду працівника: ");
                Array[i].workPlace.position = Console.ReadLine();
                Console.WriteLine("Введіть зарплату працівника: ");
                Array[i].workPlace.salary = Convert.ToInt32(Console.ReadLine());
            }
        }

        static void PrintWorker(Worker[] Array, int number)
        {
            Console.WriteLine("Прізвище та ініціали працівника: " + Array[number].name);
            Console.WriteLine("Рік початку роботи: " + Array[number].year);
            Console.WriteLine("Місяць початку роботи: " + Array[number].month);
            Console.WriteLine("Назва компанії: " + Array[number].workPlace.name);
            Console.WriteLine("Посада працівника: " + Array[number].workPlace.position);
            Console.WriteLine("Зарплата працівника: " + Array[number].workPlace.salary);
        }

        static void PrintWorkers(Worker[] Array, int number)
        {
            for (int i = 0; i < number; i++)
            {
                Console.WriteLine("Number " + (i + 1) + "\n");
                Console.WriteLine("Прізвище та ініціали працівника: " + Array[i].name);
                Console.WriteLine("Рік початку роботи: " + Array[i].year);
                Console.WriteLine("Місяць початку роботи: " + Array[i].month);
                Console.WriteLine("Назва компанії: " + Array[i].workPlace.name);
                Console.WriteLine("Посада працівника: " + Array[i].workPlace.position);
                Console.WriteLine("Зарплата працівника: " + Array[i].workPlace.salary);
                Console.WriteLine("\n\n----------------------------\n\n");
            }
        }

        static void GetWorkersInfo(Worker[] Array, int number, out int max_s, out int min_s)
        {
            max_s = 0;
            min_s = 100000;
            for(int i = 0; i<number; i++)
            {
                if(max_s > Array[i].workPlace.salary) max_s = Array[i].workPlace.salary;
                if(min_s < Array[i].workPlace.salary) min_s = Array[i].workPlace.salary;
            }
        }

        static void SortWorkerBySalary(Worker[] MyArray)
        {
            Array.Sort(MyArray, FuncSortWorkerBySalary);
        }

        static void SortWorkerByWorkExperience(Worker[] MyArray)
        {
            Array.Sort(MyArray, FuncSortWorkerByWorkExperience);
        }


        private static int FuncSortWorkerBySalary(Worker Arr1, Worker Arr2)
        {
            int salary1 = Arr1.workPlace.salary;
            int salary2 = Arr2.workPlace.salary;
            if (salary1 > salary2)
                return 1;
            if (salary1 < salary2)
                return -1;
            return 0;
        }

        private static int FuncSortWorkerByWorkExperience(Worker Arr1, Worker Arr2)
        {
            int experience1 = Arr1.GetWorkExperience();
            int experience2 = Arr2.GetWorkExperience();

            if (experience1 < experience2)
                return 1;
            if (experience1 > experience2)
                return -1;
            return 0;
        }
    }

    class Worker
    {
        private String Name;
        private int Year;
        private int Month;
        private Company WorkPlace;

        public Worker()
        {

        }

        public Worker(String Name, Company WorkPlace)
        {
            this.Name = Name;
            this.WorkPlace = WorkPlace;
        }

        public Worker(String Name, int Year, int Month, Company WorkPlace)
        {
            this.Name = Name;
            this.Year = Year;
            this.Month = Month;
            this.WorkPlace = WorkPlace;
        }

        public string name
        {
            get { return Name; }
            set { Name = value; }
        }

        public int year
        {
            get { return Year; }
            set { Year = value; }
        }

        public int month
        {
            get { return Month; }
            set { Month = value; }
        }

        public Company workPlace
        {
            get { return WorkPlace; }
            set { WorkPlace = value; }
        }

        public int GetWorkExperience()
        {
            int tempYear = DateTime.Now.Year - this.Year;
            int result = DateTime.Now.Month - this.month;
            result = result + (tempYear * 12);
            return result;
        }

        public int GetTotalMoney(Company cl)
        {
            return cl.salary * GetWorkExperience();
        }


    }

    class Company
    {
        private String Name;
        private String Position;
        private int Salary;

        public Company()
        {

        }

        public Company(String Name)
        {
            this.Name = Name;
        }

        public Company(String Name, String Position, int Salary)
        {
            this.Name = Name;
            this.Position = Position;
            this.Salary = Salary;
        }

        public string name
        {
            get { return Name; }
            set { Name = value; }
        }

        public string position
        {
            get { return Position; }
            set { Position = value; }
        }

        public int salary
        {
            get { return Salary; }
            set { Salary = value; }
        }

    }
}
